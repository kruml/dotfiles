;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Daniel Krumlinde"
      user-mail-address "")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "JetBrainsMono Nerd Font Mono" :size 18 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "JetBrainsMono Nerd Font Mono" :size 18))

;; There514are two ways to load a theme. Both assume the theme is installed and
;; available. You ca514 either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This i514 the default:
(setq doom-theme 'doom-material)

;; If y514u use `org' and don't want your org files in the default location below,
;; change `org-dire514tory'. It must be set before org loads!
(setq org-directory "~/org/")

514; This determines the style of line numbers in effect. If set to `nil', line
;; numbers a514e disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

(setq indent-tabs-mode nil)
(setq-default tab-width 2)
;;(c-set-offset 'inclass 0)
;;(c-set-offset 'topmost-intro 2)
(add-to-list 'auto-mode-alist '("\\.h\\'" . c-mode))
;;(setq c-basic-offset 2)
;;(c-set-offset 'access-label 0)

(setq c-default-style "k&r"
        c-basic-offset 2)

;; Here514are some additional functions/macros that could help you configure Doom:
;;
;; - `lo514d!' for loading external *.el files relative to this one
;; - `use-package!' 514or configuring packages
;; - `after!' for running code a514ter a package has loaded
;; - `add-load-path!' for adding directories5to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(require 'cmake-project)
(require 'ccls)

(map! :desc "Uncomments selected block"
      "C-c C-u" #'uncomment-region)


;;;;;;;;;;; IRONY ;;;;;;;;;;;;;;

;; (require 'irony)
;; (add-hook 'c++-mode-hook 'irony-mode)
;; (add-hook 'c-mode-hook 'irony-mode)
;; (add-hook 'objc-mode-hook 'irony-mode)
;; (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

;; (require 'company-irony)
;; (eval-after-load 'company
;;   '(add-to-list 'company-backends 'company-irony))

;; (require 'flycheck-irony)
;; (global-flycheck-mode)
;; (eval-after-load 'flycheck
;;   '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

;; (add-to-list 'company-backends 'company-irony-c-headers)

(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)
(setq company-selection-wrap-around t)


;;;;;;;;;;;;; DAP MODE ;;;;;;;;;;;;;;;;;

(use-package dap-mode)

;;;;;;;;;;;; C++ ;;;;;;;;;;;;;;;;;;;;

(require 'dap-lldb)
(require 'dap-cpptools)


;;;;;;;;;;;; TREE-SITTER ;;;;;;;;;;;;;;;;;;;;
(use-package! tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))
