#!/bin/bash

software_dir=$HOME/software
dotfiles_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

sudo apt install -y \
	vim \
	zsh \
	git \
	curl \
	docker \
	make \
	cmake \
	stow \
	ninja-build \
	gettext \
	unzip \
	ripgrep \
	fd-find

mkdir $software_dir 
cd $software_dir
git clone https://github.com/neovim/neovim.git

cd neovim
git checkout release-0.9
make CMAKE_BUILD_TYPE=RelWithDebInfo
cd build && cpack -G DEB && sudo dpkg -i nvim-linux64.deb

cd $dotfiles_dir
mv $HOME/.profile $HOME/.bkp_profile
mv $HOME/.zshrc $HOME/.bkp_zshrc

for dir in ./*; do
    if [ -d "$dir" ]; then
        stow -S $(basename $dir)
    fi
done

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

