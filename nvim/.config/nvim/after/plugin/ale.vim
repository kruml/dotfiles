let g:ale_linters = { 'c': ['clangtidy'] }

let g:ale_linters_explicit = 1
let g:ale_set_balloons = 1
let g:ale_hover_to_floating_preview = 1
let g:ale_floating_preview = 1

let g:ale_c_clangtidy_options = '-checks=-*,cppcoreguidelines-*'
let g:ale_c_clangtidy_checks = ['readability-*,performance-*,bugprone-*,misc-*']
let g:ale_c_clangtidy_checks += ['-readability-function-coginitive-complexity']
let g:ale_c_clangtidy_checks += ['readability-identifier-length']
let g:ale_c_clangtidy_checks += ['-misc-redundant-expression']

function! SetClangTidyConfig()
    let l:conf = findfile('.clang-tidy', expand('%:p:h').';')
    if !empty(l:conf)
        let g:ale_c_clangtidy_options = '--config=' . l:conf
    endif
endfunction

augroup ale_hover_cursor
  autocmd!
  autocmd CursorHold * ALEHover
augroup END

autocmd BufRead,BufNewFile *.c,*.h call SetClangTidyConfig()

